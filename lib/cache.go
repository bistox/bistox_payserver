package lib

import (
	"context"
	"github.com/bsm/redis-lock"
	"github.com/go-redis/redis"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"math"
	"strconv"
	"time"
)


type BlockchainCache struct {
	service *bistox_core.Service
	ctx context.Context
	lockOptions *lock.Options
	currency string
}


func NewBlockchainCache(ctx context.Context, service *bistox_core.Service) *BlockchainCache {
	const lockTimeout = 100 * time.Millisecond
	const lockRetryCount = math.MaxInt64
	const lockRetryDelay = time.Millisecond

	log.Info("Blockchain cache impl initialized")

	return &BlockchainCache{
		service: service,
		ctx: ctx,
		lockOptions: &lock.Options{
			LockTimeout: lockTimeout,
			RetryCount: lockRetryCount,
			RetryDelay: lockRetryDelay,
		},
		currency: string(service.Config.Blockchain.Options.Currency),
	}
}


type RedisLock struct {
	*lock.Locker
}


func (l RedisLock) Release(ctx context.Context) {
	select {
	case <-ctx.Done():
		return
	default:
		if l.Locker != nil {
			l.Locker.Unlock()
		}
	}
}


func (bc *BlockchainCache) ObtainLock(key string) (connectors.Lock, error) {
	lock, err := lock.Obtain(bc.service.Cache(), bc.lockKey(key), bc.lockOptions)

	if err != nil || lock == nil {
		log.Error(err)
		return nil, etc.InternalError("Can't acquire lock")
	}

	if lock == nil {
		err = etc.InternalError("Can't acquire lock")
	}

	return &RedisLock{lock}, err
}


func (bc *BlockchainCache) InsertUtxo(amount float64, txCompact string) error {
	intCmd := bc.service.Cache().ZAdd(bc.unspentKey(), redis.Z{
		Score: amount,
		Member: txCompact,
	})

	return intCmd.Err()
}


func (bc *BlockchainCache) DeleteUtxo(txCompact string) error {
	intCmd := bc.service.Cache().ZRem(bc.unspentKey(), redis.Z{
		Member: txCompact,
	})

	return intCmd.Err()
}


// Atomic "pop + push" operation
func (bc *BlockchainCache) RotateWallet(walletId string) (address string) {
	walletKey := bc.walletListKey(walletId)
	address, _ = bc.service.Cache().RPopLPush(walletKey, walletKey).Result()

	return address
}


func (bc *BlockchainCache) ExchangeBestAddress() (address string, balance int64, err error) {
	walletKey := bc.exchangeWalletKey()
	addrs, _ := bc.service.Cache().ZRevRangeByScoreWithScores(
		walletKey,
		redis.ZRangeBy{Offset: 0, Count: 1, Max: "+inf", Min: "0"},
	).Result()

	if len(addrs) == 0 {
		return "", 0, etc.InternalError("")
	}

	return addrs[0].Member.(string), int64(addrs[0].Score), err
}


func (bc *BlockchainCache) ExchangeWorstAddress() (address string, balance int64, err error) {
	walletKey := bc.exchangeWalletKey()
	addrs, err := bc.service.Cache().ZRangeByScoreWithScores(
		walletKey,
		redis.ZRangeBy{Offset: 0, Count: 1, Max: "+inf", Min: "0"},
	).Result()

	if len(addrs) == 0 {
		return "", 0, etc.InternalError("")
	}

	return addrs[0].Member.(string), int64(addrs[0].Score), err
}


func (bc *BlockchainCache) PositiveDepositAddress() (address string, balance int64, err error) {
	walletKey := bc.depositWalletKey()
	addrs, err := bc.service.Cache().ZRangeByScoreWithScores(
		walletKey,
		redis.ZRangeBy{Offset: 0, Count: 1, Max: "+inf", Min: "1"},
	).Result()

	if len(addrs) == 0 {
		return "", 0, etc.InternalError("")
	}

	return addrs[0].Member.(string), int64(addrs[0].Score), err
}


func (bc *BlockchainCache) InsertAddress(address *models.CryptoAddress) error {
	addrKey := bc.addressKey(address.Address)

	var cmdRes *redis.IntCmd

	balance := address.Balance

	var walletKey string

	if address.Wallet.Type == models.ExchangeWallet {
		walletKey = bc.exchangeWalletKey()
	} else {
		walletKey = bc.depositWalletKey()
	}

	_, err := bc.service.Cache().TxPipelined(func(pipe redis.Pipeliner) error {
		cmdRes = pipe.ZAdd(walletKey, redis.Z{
			Score: float64(balance),
			Member: address.Address,
		})

		pipe.HMSet(addrKey, map[string]interface{}{
			"wallet_id": address.WalletId,
			"balance": balance,
			"address_id": address.ID,
			"type": string(address.Wallet.Type),
		})

		return nil
	})

	if err != nil {
		return err
	}

	if cmdRes.Val() > 0 {
		if err := bc.service.Cache().LPush(bc.walletListKey(address.WalletId), address.Address).Err(); err != nil {
			return err
		}
	}

	return nil
}


func (bc *BlockchainCache) InsertWallet(wallet *models.Wallet) error {
	walletKey := bc.walletKey(wallet.ID)

	err := bc.service.Cache().HMSet(walletKey, map[string]interface{}{
		"balance": wallet.Balance,
	}).Err()

	return err
}


func (bc *BlockchainCache) GetAddress(address string) (*models.CryptoAddress, error) {
	addrKey := bc.addressKey(address)

	data, err := bc.service.Cache().HGetAll(addrKey).Result()

	if err != nil {
		return nil, err
	}

	balance, _ := strconv.ParseInt(data["balance"], 10, 64)
	id, _ := strconv.ParseUint(data["address_id"], 10, 64)

	return &models.CryptoAddress{
		Balance: balance,
		Address: address,
		WalletId: data["wallet_id"],
		ID: id,
	}, nil
}


func (bc *BlockchainCache) CheckAddress(address string) bool {
	addrKey := bc.addressKey(address)
	data := bc.service.Cache().Exists(addrKey)

	return data.Val() > 0
}


func (bc *BlockchainCache) UpdateAddressBalance(address string, amount int64) error {
	addrKey := bc.addressKey(address)

	if err := bc.service.Cache().HIncrBy(addrKey, "balance", amount).Err(); err != nil {
		return err
	}

	data := bc.service.Cache().HGet(addrKey, "type")
	err := data.Err()

	if err != nil {
		return err
	}

	addrType := models.WalletType(data.Val())
	var walletKey string

	if addrType == models.ExchangeWallet {
		walletKey = bc.exchangeWalletKey()
	} else {
		walletKey = bc.depositWalletKey()
	}

	if err := bc.service.Cache().ZIncrBy(
		walletKey,
		float64(amount),
		address).Err(); err != nil { return err }

	return nil
}


func (bc *BlockchainCache) UpdateWalletBalance(walletId string, amount int64) error {
	walletKey := bc.addressKey(walletId)

	if err := bc.service.Cache().HIncrBy(walletKey, "balance", amount).Err(); err != nil {
		return err
	}

	return nil
}


func (bc *BlockchainCache) LockMaxTxInput() (exists bool, txCompact string, amount int64) {
	unspentKey := bc.unspentKey()
	unspentLockedKey := bc.unspentLockedKey()

	unspent, _ := bc.service.Cache().ZRevRangeByScoreWithScores(
		unspentKey,
		redis.ZRangeBy{Offset: 0, Count: 1, Max: "+inf", Min: "0"},
	).Result()

	if len(unspent) == 0 {
		return false, "", 0
	}

	member, score := unspent[0].Member, unspent[0].Score
	bc.service.Cache().TxPipelined(func(pipe redis.Pipeliner) error {
		pipe.ZPopMax(unspentKey)
		pipe.ZAdd(unspentLockedKey, redis.Z{Score: score, Member: member})
		return nil
	})

	return true, member.(string), int64(score)
}


func (bc *BlockchainCache) unspentKey() string {
	return bc.currency + ":unspent"
}


func (bc *BlockchainCache) unspentLockedKey() string {
	return bc.currency + ":unspent_locked"
}


func (bc *BlockchainCache) walletListKey(walletId string) string {
	return bc.currency + ":wallet:" + walletId + ":list"
}


func (bc *BlockchainCache) walletKey(walletId string) string {
	return bc.currency + ":wallet:" + walletId
}


func (bc *BlockchainCache) exchangeWalletKey() string {
	return bc.currency + ":exchange_wallet"
}


func (bc *BlockchainCache) depositWalletKey() string {
	return bc.currency + ":deposit_wallet"
}


func (bc *BlockchainCache) addressKey(address string) string {
	return bc.currency + ":address:" + address
}


func (bc *BlockchainCache) lockKey(target string) string {
	return bc.currency + ":" + target + ":lock"
}

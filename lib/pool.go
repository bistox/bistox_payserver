package lib

import (
	"context"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"time"
)


const FreeAddressValue = 2000


type BlockchainAddressPool struct {
	service *bistox_core.Service
	ctx context.Context
	currency models.CryptoCurrency
}


func NewBlockchainAddressPool(ctx context.Context, service *bistox_core.Service) *BlockchainAddressPool {
	log.Info("Blockchain address pool initialized")

	return &BlockchainAddressPool {
		service: service,
		ctx: ctx,
		currency: service.Config.Blockchain.Options.Currency,
	}
}


func (pool *BlockchainAddressPool) Go() {
	ticker := time.NewTicker(10 * time.Second)
	db := pool.service.DB()

	for {
		cnt, err := db.Model((*models.CryptoAddress)(nil)).
			Where("wallet_id is null").
			Where("currency_code = ?", pool.currency).Count()

		if err != nil {
			log.Error(err)
			continue
		}

		for i := 0; i < FreeAddressValue - cnt; i++ {
			privateKey, _, newAddress, err := pool.service.BlockchainConnector().CreateAddress()

			if err != nil {
				log.Error(err)
				continue
			}

			addressModel := &models.CryptoAddress{
				CurrencyCode: pool.currency,
				PrivateKey: privateKey,
				Address: newAddress,
				Balance: 0,
			}

			log.Info("New free address: ", addressModel)

			if db.Insert(addressModel); err != nil {
				log.Error(err)
				continue
			}
		}

		select {
		case <-ticker.C:
		}
	}
}

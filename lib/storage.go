package lib

import (
	"context"
	"fmt"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)

var log = logging.MustGetLogger("bistox")


type BlockchainStorage struct {
	service *bistox_core.Service
	ctx context.Context
	db orm.DB
	currency models.CryptoCurrency
}


// Receives db for pg transactions
func NewBlockchainStorage(ctx context.Context, service *bistox_core.Service, db orm.DB) *BlockchainStorage {
	if db == nil {
		log.Info("Blockchain storage impl initialized")
		db = service.DB()
	}
	currency := service.BlockchainConnector().Currency()
	return &BlockchainStorage{
		service: service,
		ctx: ctx,
		db: db,
		currency: currency,
	}
}


func (s *BlockchainStorage) GetBlock(withLock bool) (*models.CryptoBlock, error) {
	block := &models.CryptoBlock{CurrencyCode: s.currency}

	q := s.db.Model(block).
		Where("currency_code = ?currency_code").
		Column("hash", "height")

	if withLock {
		q = q.For("update")
	}

	err := q.Select()

	return block, err
}


func (s *BlockchainStorage) UpdateBlock(block *models.CryptoBlock) error {
	_, err := s.db.Model(block).
		Set("hash = ?hash, height = ?height").
		Where("currency_code = ?currency_code").
		Update()

	return err
}


func (s *BlockchainStorage) PaymentsAtHeight(height uint32) ([]models.Payment, error) {
	var payments []models.Payment

	err := s.db.Model(&payments).
		Where("block_height = ? and status = ? and currency_code = ?", height, models.StatusPending, s.currency).
		Column("pm.*", "CryptoPayments", "CryptoPayments.Address.address").
		Select()

	return payments, err
}


func (s *BlockchainStorage) PreparingPayments() ([]models.Payment, error) {
	var payments []models.Payment

	err := s.db.Model(&payments).
		Where("(status = ? or status = ?) and currency_code = ?", models.StatusPreparing, models.StatusWaiting, s.currency).
		Column("pm.*", "CryptoPayments", "CryptoPayments.Address.address").
		Select()

	return payments, err
}


// Preparing -> Waiting status
func (s *BlockchainStorage) PreparePayment(payment *models.Payment) error {
	payment.Status = models.StatusWaiting
	_, err := s.db.Model(payment).
		Set("status = ?status, tx_id = ?tx_id, amount = ?amount, fee = ?fee").
		Where("id = ?id").
		Update()

	return err
}


func (s *BlockchainStorage) CheckPaymentsByTx(txId []byte, blockHeight uint32) (exist bool, err error) {
	var payments []models.Payment

	_, err = s.db.Model(&payments).
		Set("status = ?, block_height = ?", models.StatusPending, blockHeight).
		Where("tx_id = ?", txId).
		Returning("id").
		Update()

	return len(payments) > 0, err
}


func (s *BlockchainStorage) SetPaymentStatus(payment *models.Payment) error {
	_, err := s.db.Model(payment).
		Set("status = ?status").
		Where("id = ?id").
		Update()

	return err
}


func (s *BlockchainStorage) UpdateWalletBalance(walletId string, amount int64) error {
	_, err := s.db.Model(&models.Wallet{BaseModel: models.BaseModel{ID: walletId}}).
		Set("balance = balance + ?", amount).
		Where("id = ?", walletId).
		Update()

	return err
}


func (s *BlockchainStorage) UpdateAddressBalance(addressId uint64, amount int64, confirmed bool) error {
	if !confirmed && amount > 0 || confirmed && amount < 0 {
		return nil
	}

	//TODO!: extra check balance

	_, err := s.db.Model(&models.CryptoAddress{ID: addressId}).
		Set("balance = balance + ?", amount).
		Where("id = ?", addressId).
		Update()

	return err
}


func (s *BlockchainStorage) SpendTxInput(ct *models.Utxo) (address string, amount int64, err error) {
	_, err = s.db.Model(ct).
		Set("to_tx_id = ?tx_id").
		Where("tx_id = ?tx_id and vout = ?vout").
		Returning("address_id, amount").
		Update(&address, &amount)

	if err == pg.ErrNoRows {
		err = nil
	}

	return address, amount, err
}


func (s *BlockchainStorage) GetWallets(wallets *[]*models.Wallet, limit, offset int, walletTypes ...models.WalletType) error {
	where := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("w.currency_code = ?", s.service.BlockchainConnector().Currency())

		if len(walletTypes) > 0 {
			q = q.Where("w.type in (?)", pg.In(walletTypes))
		}

		return q, nil
	}

	err := s.db.Model(wallets).
		Column("w.balance", "w.id", "w.type", "Addresses").
		WhereGroup(where).
		Limit(limit).
		Offset(offset).
		Relation("Addresses", func(q *orm.Query) (*orm.Query, error) {
			return q.Column("ca.id", "ca.address", "ca.balance", "ca.wallet_id"), nil
		}).
		Select()

	return err
}


func (s* BlockchainStorage) GetCryptoTx(txId []byte) (*models.CryptoTransaction, error) {
	ct := &models.CryptoTransaction{TxId: txId}
	err := s.db.Model(ct).
		Column("tx").
		Where("tx_id = ?tx_id").
		Select()

	return ct, err
}


func (s *BlockchainStorage) GetUtxos(utxos *[]*models.Utxo, limit, offset int, height uint32, beforeHeight bool) error {
	where := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("to_tx_id is null")

		if beforeHeight {
			q = q.Where("block_height <= ?", height)
		} else {
			q = q.Where("block_height = ?", height)
		}

		return q, nil
	}

	err := s.db.Model(utxos).
		Column("utxo.tx_id", "utxo.vout", "utxo.amount", "utxo.block_height", "Address").
		WhereGroup(where).
		Order("utxo.id").
		Limit(limit).
		Offset(offset).
		Select()

	return err
}


func (s* BlockchainStorage) GetBestUtxoForUpdate(notIn []uint64, beforeHeight uint32) (*models.Utxo, error) {
	where := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("to_tx_id is null").
			Where("utxo.currency_code = ?", s.currency).
			Where("block_height <= ?", beforeHeight)

		if len(notIn) > 0 {
			q = q.Where("utxo.id not in (?)", pg.In(notIn))
		}

		return q, nil
	}

	utxo := &models.Utxo{}
	err := s.db.Model(utxo).
		Column("utxo.tx_id", "utxo.vout", "utxo.amount", "utxo.block_height", "utxo.address_id").
		ColumnExpr("ca.address AS address__address").
		Join("inner join crypto_addresses ca on ca.id = utxo.address_id").
		WhereGroup(where).
		Order("utxo.amount desc").
		For("update skip locked").
		First()

	return utxo, err
}


func (s *BlockchainStorage) PrivateKey(address string) ([]byte, error) {
	ca := &models.CryptoAddress{Address: address}
	err := s.db.Model(ca).
		Where("address = ?address").
		Column("private_key").
		Select()
	return ca.PrivateKey, err
}


func (s *BlockchainStorage) Insert(model interface{}) error {
	err := s.db.Insert(model)
	return err
}


func (s *BlockchainStorage) IncrementAddressNonce(addressId uint64) (nonce uint64, err error) {
	_, err = s.db.Model(&models.CryptoAddress{ID: addressId}).
		Set("nonce = nonce + 1").
		Where("id = ?", addressId).
		Returning("nonce").
		Update(&nonce)

	return nonce - 1, err
}


func (s *BlockchainStorage) RunInTransaction(f func(tx connectors.BlockchainStorage) error) error {
	err := s.service.DB().RunInTransaction(func(pgTx *pg.Tx) error {
		return f(NewBlockchainStorage(s.ctx, s.service, pgTx))
	})
	return err
}


func (s *BlockchainStorage) BeginTransaction() (connectors.BlockchainStorage, error) {
	db, err := s.service.DB().Begin()
	if err != nil {
		return nil, err
	}
	return NewBlockchainStorage(s.ctx, s.service, db), nil
}


func (s *BlockchainStorage) RollbackTransaction() error {
	return s.db.(*pg.Tx).Rollback()
}


func (s *BlockchainStorage) CommitTransaction() error {
	return s.db.(*pg.Tx).Commit()
}


func (s *BlockchainStorage) Notify(channel string, message string) error {
	_, err := s.db.Exec(fmt.Sprintf("notify %s, ?", channel), message)
	return err
}


type DbEventListener struct {
	*pg.Listener
}


func (l DbEventListener) Channel() <-chan *connectors.Event {
	channel := l.Listener.Channel()
	outChannel := make(chan *connectors.Event)
	go func() {
		for {
			notification := <-channel
			if notification == nil {
				continue
			}
			outChannel <- &connectors.Event{
				Channel: notification.Channel,
				Payload: notification.Payload,
			}
		}
	}()
	return outChannel
}


func (l DbEventListener) Close() {
	l.Listener.Close()
}


func (s *BlockchainStorage) Listen(channel... string) connectors.EventListener {
	listener := s.service.DB().Listen(channel...)
	return DbEventListener{listener}
}

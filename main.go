package main

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/server"
	"gitlab.com/bistox/bistox_payserver/handlers"
	"gitlab.com/bistox/bistox_payserver/lib"
	pb "gitlab.com/bistox/bistox_proto/payserver"
	"google.golang.org/grpc"
	"time"
)

//go:generate make protogen


var log = logging.MustGetLogger("bistox")


func main() {
	service := bistox_core.NewService(logging.DEBUG)
	service.Init()

	payServer := handlers.NewPayServer(&service)

	service.Config.Server.Ext = server.ConfigExt{
		RpcRegHandler: func(s *grpc.Server, mux *runtime.ServeMux) {
			pb.RegisterBistoxPayserverServer(s, &payServer)
		},
	}

	// Sync blockchain on ready
	service.OnReady(func(ctx context.Context) {
		storage := lib.NewBlockchainStorage(ctx, &service, nil)
		cache := lib.NewBlockchainCache(ctx, &service)
		pool := lib.NewBlockchainAddressPool(ctx, &service)
		// Maintain a constant value of free addresses
		go pool.Go()
        for {
		    err := connectors.SyncLoop(service.BlockchainConnector(), ctx, storage, cache)
            log.Error(err)
		    time.Sleep(time.Second)
        }
		service.Kill()
	})

	service.Run()
}

package handlers

import (
	"context"
	"github.com/go-pg/pg"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"gitlab.com/bistox/bistox_proto"
	pb "gitlab.com/bistox/bistox_proto/payserver"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var log = logging.MustGetLogger("bistox")


type PayServer struct {
	connector connectors.BlockchainConnector
	service *bistox_core.Service
}


func (p *PayServer) getConnector() connectors.BlockchainConnector {
	if p.connector == nil {
		p.connector = p.service.BlockchainConnector()  // Not thread safe, but it's ok
	}
	return  p.connector
}


func (p *PayServer) Balance(ctx context.Context, req *pb.BalanceRequest) (*pb.BalanceResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}


func (p *PayServer) CreateAddress(ctx context.Context, req *pb.CreateAddressRequest) (*pb.CreateAddressResponse, error) {
	if req.Currency.String() != string(p.getConnector().Currency()) {
		return nil, status.Error(codes.InvalidArgument, "Unsupported currency")
	}

	privateKey, _, addr, err := p.getConnector().CreateAddress()

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &pb.CreateAddressResponse{Address: addr, PrivateKey: privateKey}, err
}


func (p *PayServer) InsertAddress(ctx context.Context, address *proto.CryptoAddress) (*proto.EmptyResponse, error) {
	if err := p.getConnector().InsertAddress(&models.CryptoAddress{
		ID: address.AddressId,
		Address: address.Address,
		WalletId: address.WalletId,
		CurrencyCode: p.getConnector().Currency(),
		Wallet: &models.Wallet{
			Type: models.DepositWallet,
		},
	}); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.EmptyResponse{}, nil
}


func (p *PayServer) EstimateFee(ctx context.Context, req *pb.EstimateFeeRequest) (*pb.EstimateFeeResponse, error) {
	fee, err := p.connector.EstimateFee(req.Amount)

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &pb.EstimateFeeResponse{Fee: fee}, nil
}


func (p *PayServer) ListPayments(ctx context.Context, req *pb.ListPaymentsRequest) (*pb.ListPaymentsResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}


func (p *PayServer) PaymentByID(ctx context.Context, req *pb.PaymentByIDRequest) (*proto.Payment, error) {
	return nil, status.Error(codes.Unimplemented, "")
}


func (p *PayServer) PaymentsByAddress(ctx context.Context, req *pb.PaymentsByAddressRequest) (*pb.PaymentsByAddressResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}


func (p *PayServer) getAddress(address string) (*models.CryptoAddress, error) {
	ca := &models.CryptoAddress{Address: address}
	if err := p.service.DB().Model(ca).Where("address = ?address").Select(); err == pg.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return ca, nil
}


func (p *PayServer) CreatePayment(ctx context.Context, req *pb.PaymentRequest) (*proto.Payment, error) {
	direction := models.DirectionNone

	switch req.Direction {
	case proto.PaymentDirection_INCOMING:
		direction = models.DirectionIncoming
	case proto.PaymentDirection_INTERNAL:
		direction = models.DirectionInternal
		req.ExtAddress = ""
	case proto.PaymentDirection_OUTGOING:
		direction = models.DirectionOutgoing
	}

	if direction == models.DirectionNone || direction == models.DirectionIncoming {
		return nil, status.Error(codes.InvalidArgument, "InvalidDirection")
	}

	if req.WalletId == "" {
		return nil, status.Error(codes.InvalidArgument, "InvalidWalletId")
	}

	if direction == models.DirectionOutgoing {
		if req.ExtAddress == "" || p.getConnector().ValidateAddress(req.ExtAddress) != nil {
			return nil, status.Error(codes.InvalidArgument, "InvalidExtAddress")
		}

		if req.Amount > 0 {
			req.Amount = -req.Amount
		}
	}

	internalExtAddress, err := p.getAddress(req.ExtAddress)

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if internalExtAddress != nil {
		direction = models.DirectionInternal
	}

	payment := &models.Payment{
		Amount: req.Amount,
		Direction: direction,
		ExtAddress: req.ExtAddress,
		WalletId: req.WalletId,
		Status: models.StatusPreparing,
		CurrencyCode: p.getConnector().Currency(),
		CryptoPayments: []*models.CryptoPayment{},
	}

	if direction == models.DirectionInternal {
		payment.Status = models.StatusCompleted
	}

	// TODO: Move to common methods
	err = p.service.DB().RunInTransaction(func(tx *pg.Tx) error {
		wallet := &models.Wallet{}
		err := tx.Model(wallet).
			Where("id = ? and currency_code = ?", req.WalletId, p.connector.Currency()).
			Column("id", "balance").
			For("update").
			Select()

		if err != nil {
			return etc.Error(etc.NotFound, "Wallet is not found")
		}

		if (-payment.Amount) > wallet.Balance {
			return etc.Error(etc.InsufficientBalance, "Low balance")
		}

		_, err = tx.Model(wallet).
			Set("balance = balance + ?", payment.Amount).
			Where("id = ?", payment.WalletId).
			Update()

		if err != nil {
			return err
		}

		if internalExtAddress != nil {
			_, err = tx.Model(&models.Wallet{}).
				Set("balance = balance + ?", -payment.Amount).
				Where("id = ?", internalExtAddress.WalletId).
				Update()

			if err != nil {
				return err
			}
		}

		if err = tx.Insert(payment); err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if direction == models.DirectionOutgoing {
		p.getConnector().PaymentChannel() <- payment
	}

	return payment.ToProto(), nil
}


func (p *PayServer) ValidateAddress(ctx context.Context, req *pb.ValidateAddressRequest) (*proto.EmptyResponse, error) {
	err := p.getConnector().ValidateAddress(req.Address)

	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &proto.EmptyResponse{}, nil
}


// Force check interface implementation
var _ pb.BistoxPayserverServer = (*PayServer)(nil)


func NewPayServer(service *bistox_core.Service) PayServer {
	return PayServer{service: service}
}
